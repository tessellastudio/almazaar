;
(function ($)
{
	
	/*
	 * catalog.detail
	 * */
	var jsSlider = $('.js-slider')
	if (jsSlider.length > 0)
	{
		jsSlider.each(function ()
		{
			var $that = $(this);
			$that.bxSlider({
				adaptiveHeight: true,
				responsive    : true,
				mode          : 'fade',
				controls      : false,
				auto          : $(this).data('bx-auto') || false,
				pagerCustom   : $(this).data('bx-pager') || null
			});
			
			/* Слайдер для пагинации */
			if ($(this).data('bx-pager'))
			{
				var $bxPager = $($(this).data('bx-pager'));
				if ($bxPager.size() > 0 && $bxPager.data('bxslider'))
				{
					$($(this).data('bx-pager')).bxSlider({
						adaptiveHeight  : true,
						responsive      : true,
						minSlides       : 4,
						maxSlides       : 4,
						moveSlides      : 1,
						pager           : false,
						slideWidth      : 144,
						infiniteLoop    : true,
						slideMargin     : 20,
						hideControlOnEnd: true,
						nextText        : '',
						prevText        : '',
					});
				}
			}
		});
	}
	
	var jsCarousel = $('.js-carousel');
	if (jsCarousel.length > 0)
	{
		jsCarousel.each(function ()
		{
			
			var $that  = $(this);
			var $items = $that.data('carousel-items') || 4;
			
			$that.owlCarousel({
				items     : $items,
				singleItem: parseInt($items) == 1,
				loop      : true,
				// 				autoWidth : $that.data('carousel-autowidth') || false,
				autoHeight: $that.data('carousel-autoheight') || false,
				margin    : $that.data('carousel-margin') != undefined ? $that.data('carousel-margin') : 30,
				nav       : $that.data('carousel-nav') ? true : false,
				dots      : $that.data('carousel-dots') ? true : false,
				autoplay       : true,
				autoplayTimeout: 7000,
				smartSpeed     : 3000,
				autoplaySpeed  : 5000,
				navText   : ["<i class='ico ico_arrow-double-left'></i>", "<i class='ico ico_arrow-double-right'></i>"],
				responsive: {
					0  : {
						center    : true,
						items     : $that.data('carousel-xs-items') || $items,
						singleItem: parseInt($that.data('carousel-xs-items') || $items) == 1,
					},
					300: {
						center    : true,
						items     : $that.data('carousel-sm-items') || $items,
						singleItem: parseInt($that.data('carousel-sm-items') || $items) == 1,
					},
					600: {
						center    : true,
						items     : $that.data('carousel-md-items') || $items,
						singleItem: parseInt($that.data('carousel-md-items') || $items) == 1,
					},
					993: {
						center    : false,
						items     : $that.data('carousel-lg-items') || $items,
						singleItem: parseInt($that.data('carousel-lg-items') || $items) == 1,
					}
				}
			});
		})
	}
	
	/* Widget maps. Find "data-api" call elements and insert custom google map */
	var maps = [];
	if ((maps = $('[data-jsmap]')) && (maps.length > 0))
	{
		maps.each(function (i, map)
		{
			var coordinates = $(map).attr('data-jsmap-coordinates').split(';');
			if (typeof coordinates == 'object' && coordinates !== null && coordinates.length >= 1)
			{
				var centerPos = coordinates[0].split(',');
				var markerPos = (coordinates[1] || coordinates[0]).split(',');
				this.id       = this.id || 'mapID_' + Math.random();
				
				if ($(map).data('jsmap') == 'yandex' && 'ymaps' in window)
				{
					ymaps.ready(function ()
					{
						var myMap = new ymaps.Map(map.id, {
							center   : [centerPos[0], centerPos[1]],
							zoom     : $(map).attr('data-jsmap-zoom') || 14,
							controls : [],
							behaviors: ['drag', 'multiTouch']
						});
						
						myMap.geoObjects
							.add(new ymaps.Placemark([markerPos[0], markerPos[1]], {
									iconCaption: $(map).attr('data-jsmap-marker-title') || 'Мы тут'
								}, {
									preset             : 'islands#circleDotIconWithCaption',
									iconCaptionMaxWidth: '50',
									iconColor          : '#35CA66'
								})
							);
						
					});
					
				}
				else if ($(this).data('jsmap') == 'google' && 'google' in window)
				{
					
					var mapOptions = {
						zoom                  : parseInt($(this).attr('data-jsmap-zoom')) || 14,
						center                : new google.maps.LatLng(centerPos[0], centerPos[1]),
						mapTypeControl        : false,
						mapTypeId             : google.maps.MapTypeId.ROADMAP,
						zoomControl           : true,
						zoomControlOptions    : {
							style: google.maps.ZoomControlStyle.SMALL,
						},
						disableDoubleClickZoom: true,
						mapTypeControl        : false,
						scaleControl          : true,
						scrollwheel           : false,
						panControl            : false,
						streetViewControl     : false,
						draggable             : true,
						overviewMapControl    : false,
						disableDefaultUI      : $(this).attr('data-jsmap-disable_ui') || false,
						styles                : [
							{
								"featureType": "all",
								"elementType": "labels.text",
								"stylers"    : [
									{
										"visibility": "on"
									}
								]
							},
							{
								"featureType": "all",
								"elementType": "labels.text.fill",
								"stylers"    : [
									{
										"saturation": 36
									},
									{
										"color": "#7f7f7f"
									},
									{
										"lightness": "-12"
									},
									{
										"visibility": "on"
									}
								]
							},
							{
								"featureType": "all",
								"elementType": "labels.text.stroke",
								"stylers"    : [
									{
										"visibility": "off"
									},
									{
										"color": "#838383"
									},
									{
										"lightness": "-61"
									}
								]
							},
							{
								"featureType": "all",
								"elementType": "labels.icon",
								"stylers"    : [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "administrative",
								"elementType": "geometry.fill",
								"stylers"    : [
									{
										"color": "#000000"
									},
									{
										"lightness": 20
									}
								]
							},
							{
								"featureType": "administrative",
								"elementType": "geometry.stroke",
								"stylers"    : [
									{
										"color": "#000000"
									},
									{
										"lightness": 17
									},
									{
										"weight": 1.2
									}
								]
							},
							{
								"featureType": "landscape",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#212121"
									}
								]
							},
							{
								"featureType": "poi",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#000000"
									},
									{
										"lightness": 21
									}
								]
							},
							{
								"featureType": "road",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#eb0000"
									}
								]
							},
							{
								"featureType": "road.highway",
								"elementType": "geometry.fill",
								"stylers"    : [
									{
										"color": "#777776"
									},
									{
										"lightness": 17
									},
									{
										"visibility": "on"
									}
								]
							},
							{
								"featureType": "road.highway",
								"elementType": "geometry.stroke",
								"stylers"    : [
									{
										"weight": 0.2
									},
									{
										"color": "#717171"
									},
									{
										"visibility": "on"
									}
								]
							},
							{
								"featureType": "road.arterial",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#515050"
									},
									{
										"visibility": "on"
									}
								]
							},
							{
								"featureType": "road.local",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#383838"
									}
								]
							},
							{
								"featureType": "road.local",
								"elementType": "geometry.stroke",
								"stylers"    : [
									{
										"color": "#707070"
									}
								]
							},
							{
								"featureType": "transit",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#000000"
									},
									{
										"lightness": 19
									}
								]
							},
							{
								"featureType": "water",
								"elementType": "geometry",
								"stylers"    : [
									{
										"color": "#17191b"
									}
								]
							}
						]
					};
					if (coordinates.length == 2)
					{
						var map    = new google.maps.Map(this, mapOptions),
						    marker = new google.maps.Marker({
							    title: $(this).attr('data-jsmap-marker-title') || 'МЫ ТУТ',
							    icon : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAAA2CAMAAABk+SK1AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAACKUExURQAAAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAA8GAKGfnTYvKnp2c1NNSZeVk2diXhkQCiIaFY6LiElDPywlH4SAfnBsaV1YVEA5NMV9Yv4AAAAedFJOUwBqEbsERSYH+xvu2poaaMmcxpY72frn+Zs6lcfmaQ2BSpoAAAHGSURBVGje7dZXd4MgGAZg2qZN0713AdmO/P+/V4ZpTAWvJIecfu9FjKj4HKYIH0gQQAEKUIACFKAABShA/zG0Ijaa+f+U7FwSHPfFIf4mQn1ZOMEVGzzHqlDWbqquZoUKSinjXQTKSe2P0t5BGKVqCOUyCSX+RqzIvFD/KkPkCFrzlv2e9L4tlIg0VIQOyQEN79+FstY0aajw7RaFat/akus9QRumfDvHoVSLFJRxV2p/M0DpuOulbTJt0lDjmjQOZdoeNMsApU07mkzGvk2INNRP6jhUEWNHvZoZ6leeVo6grV0I1nwCSh02CsWisqsJZvMvTwqP11G+dt1fp6GuSRNQSox9MscYHUPtymmjuwloTWgCavupwnuCdryy0c0EFAs9nIpD6Npd3w+08eV1v8vEoarfVrFfj6QebKsU54VSl9obwujkZgKKux7KSEfXTdhVtzXmhPq4+k0/30U7BZW8f9o0hAuFc0LhexSgAAUoQAEKUIACNEDnytF5tP7bFSotpxcR590SlZezy7/M529UZI5Pdp0vj6jQLB6Gzs8VKjeDKfW2RCVnM6Veb1DhOXtyzq93VHzclLq/RgeQxcdVhuH5A8DGd6gmus+nAAAAAElFTkSuQmCC'
						    });
						marker.setMap(map);
						marker.setPosition(new google.maps.LatLng(markerPos[0], markerPos[1]));
						marker.setVisible(true);
					}
					
					if ($(this).data('jsmap-info'))
					{
						var infowindow = new google.maps.InfoWindow({
							content       : $(this).data('jsmap-info'),
							maxWidth      : 270,
							disableAutoPan: true,
							position      : new google.maps.LatLng(markerPos[0], markerPos[1]),
							pixelOffset   : new google.maps.Size(-1, 15)
						});
						infowindow.open(map, marker);
						marker.addListener('click', function ()
						{
							infowindow.open(map, marker);
						});
					}
					
				}
			}
		});
	}
	
	var $jsGallery = $('.js-gallery');
	if ($jsGallery.length > 0)
	{
		$('.js-gallery').magnificPopup({
			delegate : 'a',
			type     : 'image',
			tLoading : 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery  : {
				enabled           : true,
				navigateByImgClick: true,
				preload           : [0, 1] // Will preload 0 - before current, and 1 after the current image
			},
			image    : {
				tError  : '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function (item)
				{
					return item.el.attr('title');
				}
			}
		});
	}
	
	var $jsPopup = $('.js-popup-link');
	if ($jsPopup.length > 0)
	{
		$('.js-popup-link').magnificPopup();
	}
	
	$('.js-form').on('submit', function (event)
	{
		event.preventDefault();
		var $that = $(this);
		$that.find('.text-error').remove();
		
		$.ajax({
			url     : $that.attr('action') || window.location.url,
			method  : $that.attr('method') || 'POST',
			data    : $that.serialize(),
			dataType: $that.attr('data-jsform-data_type') || 'html'
		}).done(function (response)
		{
			if ($that.attr('data-jsform-data_type') && $that.attr('data-jsform-data_type') == 'json')
			{
				var _formConfig = response.data.config;
				
				if (response.result == 'success')
				{
					var $success = $that.data('success-selector');
					if ($success)
					{
						$that.hide();
						$success.show();
					}
					else
					{
						$.magnificPopup.open({
							items: {
								src : '#js-popup-success',
								type: 'inline'
							},
							
						}, 0);
					}
				}
				else
				{
					_formFields = response.data.fields;
					for (var field_code in _formFields)
					{
						if ('error' in _formFields[field_code])
						{
							var inputSelector = '[name="form[' + _formConfig['code'] + '][' + field_code + ']"]';
							var $input        = $(inputSelector);
							
							$input.parent().append('<div class="text-error">' + _formFields[field_code]['error'] + '</div>');
						}
					}
				}
			}
			else
			{
				
			}
		});
	});
	
	var selectbox = [];
	if ((selectbox = $('select.js-custom-select')) && selectbox.length > 0)
	{
		selectbox.ikSelect({
			syntax       : '<div class="ik_select_link"><div class="ik_select_link_inner clearfix"><span class="ik_select_link_text"></span><i class="ico ico_arrow-down ik_select_dropdown_icon"></i></div></div><div class="ik_select_dropdown"><div class="ik_select_list"></div></div>',
			// 		customClass: 'b-form-custom_select__js',
			autoWidth    : false,
			ddFullWidth  : false,
			ddCustomClass: 'text-center',
			ddMaxHeight  : 300,
			filter       : false
		});
	}
	
	var $shadowContent = $('[data-shadow-content]');
	$shadowContent.each(function ()
	{
		var cur = $(this).html();
		$(this).html($('<span></span>').addClass('no-shadow-content').html(cur));
		$('<span></span>').addClass('shadow-content').html($(this).data('shadow-content')).prependTo(this);
	});
	
	/*$(document).on(
	 'click.bs.dropdown.data-api',
	 '[data-dropdown-noclose]',
	 function (e)
	 {
	 e.stopPropagation()
	 }
	 );*/
	 
	if (document.body.clientWidth <= '1025')
	{
		jQuery(document).ready(function ($)
		{
			$('.b-navigation li:has(ul)').doubleTapToGo()
		});
	}
	
	$('.b-calendar_list a').click(function ()
	{
		get_id = $(this).attr('href');
		
		$('.b-calendar_list li').removeClass('activate');
		$(this).parent('li').addClass('activate');
		$('.b-tab__content').removeClass('activate');
		$(get_id).addClass('activate');
		
		return false;
	})
})(jQuery);
$(window).load(function ()
{
	$(".js-carousel").each(function ()
	{
		$(this).owlCarousel('refresh');
	});
	
});

;(function( $, window, document, undefined )
{
  $.fn.doubleTapToGo = function( params )
  {
    if( !( 'ontouchstart' in window ) &&
      !navigator.msMaxTouchPoints &&
      !navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;

    this.each( function()
    {
      var curItem = false;

      $( this ).on( 'click', function( e )
      {
        var item = $( this );
        if( item[ 0 ] != curItem[ 0 ] )
        {
          e.preventDefault();
          curItem = item;
        }
      });

      $( document ).on( 'click touchstart MSPointerDown', function( e )
      {
        var resetItem = true,
          parents    = $( e.target ).parents();

        for( var i = 0; i < parents.length; i++ )
          if( parents[ i ] == curItem[ 0 ] )
            resetItem = false;

        if( resetItem )
          curItem = false;
      });
    });
    return this;
  };
})( jQuery, window, document );

