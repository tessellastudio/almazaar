var gulp           = require('gulp'), // Сообственно Gulp JS
    rigger         = require('gulp-rigger'),
    //    concat      = require('gulp-concat'), // Склейка файлов
    plumber        = require('gulp-plumber'),
    browserSync    = require('browser-sync').create();
var deployPathRoot = '../../../OpenServer/domains/platinum.dev/local/templates/.default/';
var
    __path         = {
	    build : { //Тут мы укажем куда складывать готовые после сборки файлы
		    html     : 'build/',
		    js       : 'build/js/',
		    css      : 'build/css/',
		    img      : 'build/i/',
		    bg_images: 'build/i/bg/',
		    sprite   : 'build/i/bg/',
		    fonts    : 'build/fonts/'
	    },
	    src   : { //Пути откуда брать исходники
		    html  : 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
		    js    : 'src/js/script.js',//В стилях и скриптах нам понадобятся только main файлы
		    style : ['src/style/*.less'],
		    img   : ['./src/i/**/*.*', '!./src/i/sprite/**/*.*'], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
		    sprite: 'src/i/sprite/**/*.*',
		    fonts : 'src/fonts/**/*.*'
	    },
	    watch : { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
		    html  : 'src/**/*.html',
		    js    : 'src/js/**/*',
		    style : 'src/style/**/*.less',
		    img   : ['src/i/**/*.*', '!./src/i/sprite/**/*.*'],
		    sprite: 'src/i/sprite/**/*.*',
		    fonts : 'src/fonts/**/*.*'
	    },
	    deploy: {
		    js    : {from: 'build/js/**/*.*', to: deployPathRoot + '/js/'},
		    css   : {from: ['build/css/*.css'], to: deployPathRoot},
		    images: {from: 'build/i/**/*.*', to: deployPathRoot + '/i/'},
		    fonts : {from: 'build/fonts/**/*.*', to: deployPathRoot + '/fonts/'}
	    },
	    clean : './build'
    };

// HTML
gulp.task('html', function ()
{
	gulp.src(__path.src.html) //Выберем файлы по нужному пути
		.pipe(plumber())
		.pipe(rigger()) //Прогоним через rigger
		.pipe(gulp.dest(__path.build.html)) //Выплюнем их в папку build
		.pipe(browserSync.stream()); //И перезагрузим наш сервер для обновлений
});

// LESS
gulp.task('style', function ()
{
	var path                 = require('path'),
	    less                 = require('gulp-less'), // Плагин для Less
	    csso                 = require('gulp-csso'),
	    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	    autoprefix           = new LessPluginAutoPrefix({browsers: ["last 3 versions"]});
	console.log('less begin');
	
	return gulp.src(__path.src.style)
		.pipe(plumber())
		.pipe(less({
			paths  : [path.join(__dirname, 'less')],
			plugins: [autoprefix]
		}))
		.pipe(csso({
			restructure: false,
			sourceMap  : false,
			debug      : true
		}))//Сожмем
		.pipe(gulp.dest(__path.build.css)) //И в build
		.pipe(browserSync.stream());
	// даем команду на перезагрузку css
});

// JAVASCRIPT
gulp.task('js', function ()
{
	var uglify = require('gulp-uglify'); // Минификация JS
	gulp.src(__path.src.js) //Найдем наш main файл
		.pipe(plumber())
		.pipe(rigger()) //Прогоним через rigger
		.pipe(uglify()) //Сожмем наш js
		.pipe(gulp.dest(__path.build.js)) //Выплюнем готовый файл в build
		.pipe(browserSync.stream()); // даем команду на перезагрузку страницы
});

// IMAGES MIN
gulp.task('images', function ()
{
	var changed = require('gulp-changed');
	gulp.src(__path.src.img) //Выберем наши картинки
		.pipe(plumber())
		.pipe(changed(__path.build.img))
		.pipe(gulp.dest(__path.build.img)) //И бросим в build
		.pipe(browserSync.stream());
	return;
	
	var imagemin = require('gulp-imagemin'), // Минификация изображений
	    pngquant = require('imagemin-pngquant');
	gulp.src(__path.src.img) //Выберем наши картинки
		.pipe(plumber())
		.pipe(imagemin({ //Сожмем их
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use        : [pngquant()],
			interlaced : true
		}))
		.pipe(gulp.dest(__path.build.img)) //И бросим в build
		.pipe(browserSync.stream());
	
});

// FONTS
gulp.task('fonts', function ()
{
	gulp.src(__path.src.fonts)
		.pipe(gulp.dest(__path.build.fonts))
});

// HTTP SERVER
gulp.task('serve', function ()
{
	
	browserSync.init({
		server: "./build",
// 		tunnel: "platinum"
		// 		bro
		// wser: 'chrome'
	});
	
	gulp.watch([__path.watch.html], ['html']);
	gulp.watch([__path.watch.style], function ()
	{
		gulp.run(['style']);
	});
	gulp.watch([__path.watch.js], function ()
	{
		gulp.run(['js']);
	});
	gulp.watch([__path.watch.sprite], function ()
	{
		gulp.run(['sprite']);
	});
});

// BOWER COMPONENTS
gulp.task('bower', function ()
{
	var uglify = require('gulp-uglify'); // Минификация JS
	var arJs   = [
		'./bower_components/jquery/dist/jquery.min.js',
		'./bower_components/bootstrap/dist/js/bootstrap.min.js',
		'./bower_components/owl.carousel/dist/owl.carousel.min.js',
		'./bower_components/bxslider-4/dist/jquery.bxslider.min.js',
		'./bower_components/bxslider-4/dist/vendor/*.*',
		'./bower_components/ikselect/dist/jquery.ikSelect.min.js',
		'./bower_components/magnific-popup/dist/jquery.magnific-popup.min.js',
	];
	gulp.src(arJs)
		.pipe(uglify())
		.pipe(gulp.dest(__path.build.js));
	
	gulp.src([
		'./bower_components/font-awesome/fonts/*.*'
	])
		.pipe(gulp.dest('./build/fonts/font-awesome/'));
	
	// bxslider
	gulp.src([
		'./bower_components/bxslider-4/dist/images/*.*'
	])
		.pipe(gulp.dest('./build/i/bg/bx-slider/'));
});

gulp.task('sprite', function ()
{
	var spritesmith = require('gulp.spritesmith'),
	    spriteData  = gulp.src(__path.src.sprite) // путь, откуда берем картинки для спрайта
		    .pipe(spritesmith({
			    imgName    : 'sprite.png',
			    cssName    : 'sprite.less',
			    algorithm  : 'top-down',//'binary-tree',
			    imgPath    : '../i/bg/sprite.png',
			    cssTemplate: 'less.template.spritesmith',
			    cssVarMap  : function (sprite)
			    {
				    sprite.name = 'ico_' + sprite.name
			    }
		    }));
	
	spriteData.img.pipe(gulp.dest(__path.build.sprite)); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./src/style/lib')); // путь, куда сохраняем стили
});

gulp.task('deploy', function ()
{
	return false;
	var replace = require('gulp-replace');
	
	gulp.src(__path.deploy.js.from)
		.pipe(gulp.dest(__path.deploy.js.to));
	
	gulp.src(__path.deploy.css.from)
		.pipe(replace(/\.{0,2}\/(i\/bg|fonts)/g, '/local/templates/.default/$1'))
		.pipe(gulp.dest(__path.deploy.css.to));
	
	gulp.src(__path.deploy.images.from)
		.pipe(gulp.dest(__path.deploy.images.to));
	gulp.src(__path.deploy.fonts.from)
		.pipe(gulp.dest(__path.deploy.fonts.to));
});

gulp.task('build', [
	'bower',
	'fonts',
	'sprite',
	'bower',
	'html',
	'style',
	'js',
	'images',
]);

gulp.task('clean', function (cb)
{
	rimraf = require('rimraf'),
		rimraf(__path.clean, cb);
});

gulp.task('default', ['build', 'serve']);
